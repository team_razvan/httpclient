

#ifndef RTBIMPLNGHTTP2_H
#define RTBIMPLNGHTTP2_H

#include "RTBImplInterface.h"

#include <nghttp2/asio_http2_client.h>

#include <future>
#include <condition_variable>
#include <mutex>
#include <thread>

using boost::asio::ip::tcp;

using namespace nghttp2::asio_http2;
using namespace nghttp2::asio_http2::client;

class RTBClientImplNghttp2 : public RTBClientImplInterface {
public:
    RTBClientImplNghttp2();
    virtual ~RTBClientImplNghttp2() = default;

    RTBRequest createRequest() override;
    void stop() override;

private:
    boost::asio::io_service m_ioService;
    session m_session;

    bool m_connected;
    std::mutex m_connectedMutex;
    std::condition_variable m_connectedCV;
    std::thread m_thread;
};

class RTBRequestImplNghttp2 : public RTBRequestImplInterface {
public:
    RTBRequestImplNghttp2(session& session);
    virtual ~RTBRequestImplNghttp2() = default;

    RTBPromise sendRequest() override;
    void setMethod() override;
    void setURI() override;
    void setRTBVersionHeader() override;
    void setBody() override;

private:
    session& m_session;
};

class RTBPromiseImplNghttp2 : public RTBPromiseImplInterface {
public:
    RTBPromiseImplNghttp2(const request* request);
    virtual ~RTBPromiseImplNghttp2() = default;

    bool isDone() override;
    RTBResponse get() override;

private:
    std::atomic_bool m_ready;
    std::future<RTBResponse> m_futureResponse;
};

class RTBResponseImplNghttp2 : public RTBResponseImplInterface {
public:
    RTBResponseImplNghttp2(const char* body, std::size_t len);
    virtual ~RTBResponseImplNghttp2() = default;

    std::string getBody() override;

private:
    std::string m_body;
};

#endif

