
#ifndef RTBIMPLINTERFACE_H
#define RTBIMPLINTERFACE_H

#include "RTB.h"

class RTBClientImplInterface {
public:
    virtual ~RTBClientImplInterface() = default;

    virtual RTBRequest createRequest() = 0;
    virtual void stop() = 0;
};

class RTBRequestImplInterface {
public:
    virtual ~RTBRequestImplInterface() = default;

    virtual RTBPromise sendRequest() = 0;
    virtual void setMethod() = 0;
    virtual void setURI() = 0;
    virtual void setRTBVersionHeader() = 0;
    virtual void setBody() = 0;
};

class RTBPromiseImplInterface {
public:
    virtual ~RTBPromiseImplInterface() = default;

    virtual bool isDone() = 0;
    virtual RTBResponse get() = 0;
};

class RTBResponseImplInterface {
public:
    virtual ~RTBResponseImplInterface() = default;

    virtual std::string getBody() = 0;
};

#endif

