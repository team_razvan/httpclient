
#include "RTBImplNghttp2.h"

RTBClientImplNghttp2::RTBClientImplNghttp2()
    : m_ioService()
    , m_session(m_ioService, "nghttp2.org", "80")
    , m_connected(false)
    , m_connectedMutex()
    , m_connectedCV()
    , m_thread([&](){
          m_session.on_connect([&](tcp::resolver::iterator endpoint_it) {
              {
                  std::lock_guard<std::mutex> lk(m_connectedMutex);
                  m_connected = true;
                  std::cout << "Connected" << std::endl;
              }
              m_connectedCV.notify_all();
          });
          m_session.on_error([](const boost::system::error_code &ec) {
              std::cerr << "error: " << ec.message() << std::endl;
          });
          m_ioService.run();
      }) {
    std::unique_lock<std::mutex> lk(m_connectedMutex);
    m_connectedCV.wait(lk, [&]() {return m_connected;});
}

RTBRequest RTBClientImplNghttp2::createRequest() {
    return RTBRequest(new RTBRequestImplNghttp2(m_session));
}

void RTBClientImplNghttp2::stop() {
    m_session.shutdown();
    m_thread.join();
}

RTBRequestImplNghttp2::RTBRequestImplNghttp2(session& session) : m_session(session){
}

RTBPromise RTBRequestImplNghttp2::sendRequest() {
    boost::system::error_code ec;
    auto req = m_session.submit(ec, "GET", "http://nghttp2.org:80/httpbin/get");

    return RTBPromise(new RTBPromiseImplNghttp2(req));
}

void RTBRequestImplNghttp2::setMethod() {
}

void RTBRequestImplNghttp2::setURI() {
}

void RTBRequestImplNghttp2::setRTBVersionHeader() {
}

void RTBRequestImplNghttp2::setBody() {
}

RTBPromiseImplNghttp2::RTBPromiseImplNghttp2(const request* req) : m_ready(false), m_futureResponse() {
    auto promise = std::make_shared<std::promise<RTBResponse>>();
    m_futureResponse = promise->get_future();

    auto body = std::make_shared<std::string>();

    req->on_response([&, body](const response &res) mutable {
        res.on_data([&, body](const uint8_t *data, std::size_t len) mutable {
            body->append(std::string((const char*)data, len));
        });
     });

    req->on_close([&, promise, body](uint32_t error_code) mutable {
        m_ready.store(true);
        promise->set_value(RTBResponse(new RTBResponseImplNghttp2(body->c_str(), body->length())));
    });
}

bool RTBPromiseImplNghttp2::isDone() {
    return m_ready.load();
}

RTBResponse RTBPromiseImplNghttp2::get() {
    return m_futureResponse.get();
}

RTBResponseImplNghttp2::RTBResponseImplNghttp2(const char* body, std::size_t len) : m_body(body, len) {
}

std::string RTBResponseImplNghttp2::getBody() {
    return m_body;
}

