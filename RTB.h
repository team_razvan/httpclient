
#ifndef RTB_H
#define RTB_H

#include <memory>
#include <string>

class RTBClientImplInterface;
class RTBRequestImplInterface;
class RTBPromiseImplInterface;
class RTBResponseImplInterface;

class RTBRequest;

class RTBClient {
public:
    RTBClient(const std::string& impl);
    ~RTBClient();

    RTBRequest createRequest();
    void stop();

private:
    std::unique_ptr<RTBClientImplInterface> m_impl;
};

class RTBPromise;

class RTBRequest {
public:
    RTBRequest(RTBRequestImplInterface* impl);
    RTBRequest(RTBRequest&& other);
    ~RTBRequest();

    RTBPromise sendRequest();
    void setMethod();
    void setURI();
    void setRTBVersionHeader();
    void setBody();

private:
    std::unique_ptr<RTBRequestImplInterface> m_impl;
};

class RTBResponse;

class RTBPromise {
public:
    RTBPromise(RTBPromiseImplInterface* impl);
    RTBPromise(RTBPromise&& other);
    ~RTBPromise();

    bool isDone();
    RTBResponse get();

private:
    std::unique_ptr<RTBPromiseImplInterface> m_impl;
};

class RTBResponse {
public:
    RTBResponse(RTBResponseImplInterface* impl);
    RTBResponse(RTBResponse&& other);
    ~RTBResponse();

    std::string getBody();

private:
    std::unique_ptr<RTBResponseImplInterface> m_impl;
};

#endif

