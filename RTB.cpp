
#include "RTB.h"
#include "RTBImplPistache.h"
#include "RTBImplNghttp2.h"

RTBClient::RTBClient(const std::string& impl) : m_impl() {
    if (impl == "Pistache") {
        m_impl.reset(new RTBClientImplPistache());
    } else if (impl == "nghttp2") {
        m_impl.reset(new RTBClientImplNghttp2());
    }
}

RTBClient::~RTBClient() {
}

RTBRequest RTBClient::createRequest() {
    return m_impl->createRequest();
}

void RTBClient::stop() {
    m_impl->stop();
}

RTBRequest::RTBRequest(RTBRequestImplInterface* impl) : m_impl(impl) {
}

RTBRequest::RTBRequest(RTBRequest&& other) : m_impl(std::move(other.m_impl)) {
}

RTBRequest::~RTBRequest() {
}

RTBPromise RTBRequest::sendRequest() {
    return m_impl->sendRequest();
}
void RTBRequest::setMethod() {
    m_impl->setMethod();
}
void RTBRequest::setURI() {
    m_impl->setURI();
}
void RTBRequest::setRTBVersionHeader() {
    m_impl->setRTBVersionHeader();
}
void RTBRequest::setBody() {
    m_impl->setBody();
}

RTBPromise::RTBPromise(RTBPromiseImplInterface* impl) : m_impl(impl) {
}

RTBPromise::RTBPromise(RTBPromise&& other) : m_impl(std::move(other.m_impl)) {
}

RTBPromise::~RTBPromise() {
}

bool RTBPromise::isDone() {
    return m_impl->isDone();
}

RTBResponse RTBPromise::get() {
    return m_impl->get();
}

RTBResponse::RTBResponse(RTBResponseImplInterface* impl) : m_impl(impl) {
}

RTBResponse::RTBResponse(RTBResponse&& other) : m_impl(std::move(other.m_impl)) {
}

RTBResponse::~RTBResponse() {
}

std::string RTBResponse::getBody() {
    return m_impl->getBody();
}

