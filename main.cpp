#include "RTB.h"

#include <iostream>
#include <list>
#include <thread>

RTBPromise getRTBPromise(RTBClient& rtbClient) {
    RTBRequest rtbRequest = rtbClient.createRequest();
    return rtbRequest.sendRequest();
}

int main() {
    RTBClient rtbClient("Pistache");
    //RTBClient rtbClient("nghttp2");

    const int reqNb = 4;
    std::list<std::thread> promises;

    for(int i = 0; i < reqNb; ++i) {
        promises.emplace_back([&] () {
                RTBPromise rtbPromise = getRTBPromise(rtbClient);
                while (!rtbPromise.isDone());
                RTBResponse rtbResponse = rtbPromise.get();
                std::cout << "Response:" << std::endl << std::endl;
                std::cout << rtbResponse.getBody() << std::endl;
        });
    }

    for (auto& thread : promises) {
        thread.join();
    }

    rtbClient.stop();

    return 0;
}
