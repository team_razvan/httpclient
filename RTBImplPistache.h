
#ifndef RTBIMPLPISTACHE_H
#define RTBIMPLPISTACHE_H

#include "RTBImplInterface.h"

#include "pistache/net.h"
#include "pistache/http.h"
#include "pistache/client.h"

#include <future>

using namespace Net;

class RTBClientImplPistache : public RTBClientImplInterface {
public:
    RTBClientImplPistache();
    virtual ~RTBClientImplPistache() = default;

    RTBRequest createRequest() override;
    void stop() override;

private:
    Http::Client m_client;
};

class RTBRequestImplPistache : public RTBRequestImplInterface {
public:
    RTBRequestImplPistache(Http::RequestBuilder&& builder) : m_requestBuilder(std::move(builder)) {}
    virtual ~RTBRequestImplPistache() = default;

    RTBPromise sendRequest() override;
    void setMethod() override;
    void setURI() override;
    void setRTBVersionHeader() override;
    void setBody() override;

private:
    Http::RequestBuilder m_requestBuilder;
};

class RTBResponseImplPistache;

class RTBPromiseImplPistache : public RTBPromiseImplInterface {
public:
    RTBPromiseImplPistache(Async::Promise<Http::Response>&& p);
    virtual ~RTBPromiseImplPistache() = default;

    bool isDone() override;
    RTBResponse get() override;

private:
    Async::Promise<Http::Response> m_promise;
    std::future<RTBResponse> m_futureResponse;
};

class RTBResponseImplPistache : public RTBResponseImplInterface {
public:
    RTBResponseImplPistache(Http::Response&& response) : m_response(std::move(response)) {
    }
    virtual ~RTBResponseImplPistache() = default;

    std::string getBody() override;

private:
    Http::Response m_response;
};

#endif

