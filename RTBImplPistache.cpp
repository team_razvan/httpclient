
#include "RTBImplPistache.h"

RTBClientImplPistache::RTBClientImplPistache() : m_client() {
    auto opts = Http::Client::options()
        .threads(1)
        .maxConnectionsPerHost(8);
    m_client.init(opts);
}

RTBRequest RTBClientImplPistache::createRequest() {
    return RTBRequest(new RTBRequestImplPistache(m_client.get("http://nghttp2.org/httpbin/get")));
}

void RTBClientImplPistache::stop() {
    m_client.shutdown();
}

RTBPromise RTBRequestImplPistache::sendRequest() {
    return RTBPromise(new RTBPromiseImplPistache(m_requestBuilder.send()));
}

void RTBRequestImplPistache::setMethod() {
}

void RTBRequestImplPistache::setURI() {
}

void RTBRequestImplPistache::setRTBVersionHeader() {
}

void RTBRequestImplPistache::setBody() {
}

RTBPromiseImplPistache::RTBPromiseImplPistache(Async::Promise<Http::Response>&& p) : m_promise(std::move(p)) {
    auto promiseResponse = std::make_shared<std::promise<RTBResponse>>();
    m_futureResponse = promiseResponse->get_future();

    m_promise.then([&, promiseResponse] (Http::Response response) mutable {
                       promiseResponse->set_value(RTBResponse(new RTBResponseImplPistache(std::move(response))));
                   }, [&, promiseResponse] (std::exception_ptr e) {
                       promiseResponse->set_exception(e);
                   }
            );
}

bool RTBPromiseImplPistache::isDone() {
    return m_promise.isFulfilled();
}

RTBResponse RTBPromiseImplPistache::get() {
    return m_futureResponse.get();
}

std::string RTBResponseImplPistache::getBody() {
    return m_response.body();
}

